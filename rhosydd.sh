#!/bin/sh
set -x
BACKEND_SERVICES="rhosydd-mock-backend.service rhosydd-speedo-backend.service"
backend1="mock"
backend2="speedo"
expected_vehicles="$backend1
$backend2"
signal_pattern='^[[:alnum:].]+ = [-+]?[0-9]*\.?[0-9]+±[0-9]+\.[0-9]+ \(R, last updated: [0-9]+µs ago\)$'
metadata_pattern='^[[:alnum:].]+ \(R\): \{.*\}$'
backend1_signal=""
backend2_signal=""
passed=0
failed=0
# pass NAME [MESSAGE...]
pass() {
    passed=$((passed + 1))
    echo "$1:" "pass"
}

# fail NAME [MESSAGE...]
fail() {
    failed=$((failed + 1))
    echo "$1:" "fail"
}

get_backend_attribute() {

    if [ -n "$2" ]; then

        get_attr=$(rhosydd-client get-attribute $1 $2)
        exit_code=$?
        if [ $exit_code -eq 0 ] && printf '%s\n' "$get_attr" | grep -qE 'µs ago\)$'; then
            echo "0"
        else
            echo "get-attribute $1 $2 failed with exit code $exit_code or invalid output"
            echo "1"
        fi

    else
        echo "Signal list is empty"
        echo "1"
    fi
}

###############################
# Start mock backends, check the status and check output of list-vehicles
test1() {
    test_name=backend_availability

    for service in $BACKEND_SERVICES; do
        # Check if the service is active
        systemctl start "$service"
        sleep 1
        if systemctl is-active --quiet "$service"; then
            continue
        else
            echo "Service $service is not active."
            fail $test_name
        fi
    done

    list_vehicles=$(rhosydd-client list-vehicles)

    exit_code=$?

    if [ $exit_code -eq 0 ]; then

        if [ "$expected_vehicles" = "$list_vehicles" ]; then
            pass $test_name
        else
            fail $test_name
        fi

    else

        echo "Error returned from list-vehicles with code $exit_code"
        fail $test_name

    fi

}

###############################
#Test case for show-vehicle and show-vehicle-metadata
test2() {
    test_name="$1_for_$2"
    show_vehicle=$(rhosydd-client $1 $2)

    exit_code=$?

    if [ $exit_code -eq 0 ]; then

        if [ "$1" = "show-vehicle" ]; then
            pattern=$signal_pattern
        elif [ "$1" = "show-vehicle-metadata" ]; then
            pattern=$metadata_pattern
        else
            echo "Invalid command"
            fail $test_name
        fi

        if printf '%s\n' "$show_vehicle" | grep -qE "$pattern"; then
            signal_name=$(printf '%s\n' "$show_vehicle" | awk '{print $1}')

            case "$2" in
            "mock")
                backend1_signal=$signal_name
                ;;
            "speedo")
                backend2_signal=$signal_name
                ;;
            esac

            pass $test_name

        else
            echo "Invalid output received from $1 $2"
            fail $test_name
        fi

    else

        echo "Error returned from $1 $2 with code $exit_code"
        fail $test_name

    fi

}

###############################
#Test for get-attribute
test3() {
    test_name=get_attribute_for_$1

    case "$1" in
    "mock")
        ret=$(get_backend_attribute $1 ${backend1_signal%% *} | tail -n 1)
        ;;
    "speedo")
        ret=$(get_backend_attribute $1 ${backend2_signal%% *} | tail -n 1)
        ;;
    *)
        echo "Invalid backend"
        fail $test_name
        ;;
    esac

    if [ $ret = "0" ]; then
        pass $test_name
    else
        fail $test_name
    fi
}

test_for() {
    test2 show-vehicle $1
    test2 show-vehicle-metadata $1
    test3 $1
}

#Execution of test cases
test1
test_for mock
test_for speedo

echo "No of test cases passed : $passed"
echo "No of test cases failed : $failed"

set +x
exit $failed
